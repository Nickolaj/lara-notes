install:
	composer install
	npm install
	php artisan migrate
	php artisan key:generate
	php artisan serve
