<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $countInPage = 4;
        return view('note.home', [
            'articles' => Article::orderBy('created_at', 'desc')
                ->where('published', 1)
                ->paginate($countInPage)
        ]);
    }
}
